import React, { Component } from 'react';

import { connect } from 'react-redux';
import { changeTitle, changeColor } from './actions/'

export const upperCaseWrapper = (WrappedComponent) => {
    return class SomethingOther extends Component {
        render() {
            const props = Object.assign({}, this.props, { title: (this.props.title || 'No Title').toUpperCase() });

            return <WrappedComponent { ...props } />
        }
    }
}

export const colorizeComponent = (WrappedComponent, color) => {
    return class SomethingOtherThanBefore extends Component {
        render() {
            return (
                <div>
                    <WrappedComponent  { ...this.props } style={{ color }} />
                </div>
            );
        }
    }
}

export const withRedux = (WrappedComponent) => {
    class WithReduxComponent extends Component {
        constructor(){
            super();

            this.changeTitle = this.changeTitle.bind(this);
            this.changeColor = this.changeColor.bind(this);
        }

        changeTitle(title){
            this.props.changeTheTitle(title);
        } 
        
        changeColor(color){
            this.props.changeTheColor(color);
        }
 
        render() {
            return (
                <div>
                    <WrappedComponent {...this.props} changeTitle={this.changeTitle} changeColor={this.changeColor} />
                </div>
            );
        }
    }

    const mapStateToProps = (state) => {
        return { title: state.title, color: state.color }
    }

    const mapDispatchToProps = dispatch => {
       return {
            changeTheTitle: title => {
                dispatch(changeTitle(title));
            },
            changeTheColor: color => {
                dispatch(changeColor(color));
            }
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(WithReduxComponent);
}




