import React, { Component } from 'react';

export default class HelloWorld extends Component {
	createInputsForColors(colors) {
		let inputColors = [];
		colors.map((color, index) => inputColors.push(
			<div style={{ color }} key={index} className="color-input">
				<input defaultChecked={this.props.color === color} type="radio" name="color" onClick={_ => this.props.changeColor(color)} id={color} />
				<label htmlFor={color}>{color.toLocaleUpperCase()}</label>
			</div>
		)
		);
		return inputColors;
	}

	render() {
		let colors = ['blue', 'red', 'yellow', 'purple', 'green'];
		return (
			<div style={Object.assign({}, this.props.style, { color: this.props.color })} >
				<input type="text" onChange={e => this.props.changeTitle(e.target.value)} />
				<h3>{this.props.title}</h3>
				<h3>{this.props.film}</h3>
				{this.createInputsForColors(colors)}
			</div>
		);
	}
}