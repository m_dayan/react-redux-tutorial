import React, { Component } from 'react';
import { createStore } from 'redux';

import logo from './logo.svg';
import './App.css';

import { withRedux,colorizeComponent,upperCaseWrapper } from './HOCs';
import HelloWorld from './components/HelloWorld';

import setTitle from './reducers/';

const store = createStore(setTitle);

class App extends Component {
 
  render() {
     
    const UpperCaseColoredComponent = withRedux(colorizeComponent(upperCaseWrapper(HelloWorld),'red'),store);
    return (
     
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <UpperCaseColoredComponent film="Star Treck" />
      </div>       
    );
  }
}

export default App;
