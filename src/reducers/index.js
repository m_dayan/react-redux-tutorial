import { combineReducers } from 'redux'

const title = (state='HELLO WORLD',action) =>{
       
    if(action.type === 'CHANGE_TITLE'){        
        let title = action.title || 'HELLO WORLD';
        return state = title;
    }
    return state;
}

const color = (state='red',action)=>  {
    if(action.type === 'CHANGE_COLOR'){
        return state = action.color;
    }
    return state;
}

const setTitle = combineReducers({
    title,
    color,        
    });

export default setTitle;